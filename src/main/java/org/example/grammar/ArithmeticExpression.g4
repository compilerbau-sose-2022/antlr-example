grammar ArithmeticExpression;

expr
    :   multExpr ((PLUS | MINUS) multExpr)*
    ;

multExpr
    :   atom ((STAR | SLASH) atom)*
    ;

atom
    :   NUMBER
    |   LPAREN expr RPAREN
    ;

PLUS    : '+';
MINUS   : '-';
STAR    : '*';
SLASH   : '/';
LPAREN  : '(';
RPAREN  : ')';

NUMBER : [0-9]+ ('.' [0-9]+)?;
WS     : [ \t\r\n]+ -> skip;

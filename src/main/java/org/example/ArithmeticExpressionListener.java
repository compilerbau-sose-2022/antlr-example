package org.example;// Generated from ArithmeticExpression.g4 by ANTLR 4.12.0
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ArithmeticExpressionParser}.
 */
public interface ArithmeticExpressionListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ArithmeticExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(ArithmeticExpressionParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArithmeticExpressionParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(ArithmeticExpressionParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArithmeticExpressionParser#multExpr}.
	 * @param ctx the parse tree
	 */
	void enterMultExpr(ArithmeticExpressionParser.MultExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArithmeticExpressionParser#multExpr}.
	 * @param ctx the parse tree
	 */
	void exitMultExpr(ArithmeticExpressionParser.MultExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ArithmeticExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(ArithmeticExpressionParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link ArithmeticExpressionParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(ArithmeticExpressionParser.AtomContext ctx);
}